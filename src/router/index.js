import Vue from 'vue'
import Router from 'vue-router'
import MainPage from '@/page/mainPage/lightbrother/mainPage'
import Login from '@/page/login/yyl/login'
import Register from '@/page/login/yyl/register'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'mainPage',
      component: MainPage
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/register',
      component: Register
    }
  ]
})
